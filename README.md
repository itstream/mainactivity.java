package num.edu.kh.customlistview;

import android.content.Intent;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView todoTitleItem,todoDateItem;
    private CheckBox todoDoneItem;
    private CustomAdapter mCustomAdapter;
    private ListView listviewTodo;
    private SimpleDateFormat df ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        df = new SimpleDateFormat("dd MMM, yyyy");

        listviewTodo = (ListView) findViewById(R.id.todo_list_view);
        final ArrayList<Todo> myList = TodoSet.getInstance().getTodoList();
        mCustomAdapter = new CustomAdapter(myList);
        listviewTodo.setAdapter(mCustomAdapter);

//        listviewTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Toast.makeText(MainActivity.this,
//                        "Clicked["+myList.get(position).getTitle()+"]",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mCustomAdapter.notifyDataSetChanged();
    }

    class CustomAdapter extends BaseAdapter{
        private ArrayList<Todo> mTodoList;

        public CustomAdapter(ArrayList<Todo> list){
            mTodoList = list;
        }

        @Override
        public int getCount() {
            return mTodoList.size();
        }

        @Override
        public Object getItem(int position) {
            return mTodoList.get(position);
        }

        @Override
        public long getItemId(int id) {
            return id;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.single_list_item,null);
            todoTitleItem = view.findViewById(R.id.todo_title_item);
            todoDateItem = view.findViewById(R.id.todo_date_item);
            todoDoneItem = view.findViewById(R.id.todo_done_item);

            todoTitleItem.setText(mTodoList.get(position).getTitle());
            todoDateItem.setText(df.format(mTodoList.get(position).getDate()));
            todoDoneItem.setChecked(mTodoList.get(position).getDone());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(MainActivity.this,
//                            "clicked["+mTodoList.get(position).getTitle()+"]",
//                            Toast.LENGTH_SHORT).show();
                    Log.i("TODO","click at todoId "+mTodoList.get(position).getTodoId());
                    Intent editTodoActivity =
                            new Intent(MainActivity.this,EditTodoActivity.class);
                        editTodoActivity.putExtra("TODO_ID",mTodoList.get(position).getTodoId());
                    startActivity(editTodoActivity);
                }
            });

            return view;
        }
    }

}
